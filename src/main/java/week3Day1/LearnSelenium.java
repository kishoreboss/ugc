package week3Day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnSelenium {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Css");
		driver.findElementById("createLeadForm_firstName").sendKeys("Kishore");
		driver.findElementById("createLeadForm_lastName").sendKeys("Deva");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select dropDown = new Select(src); 
		dropDown.selectByVisibleText("Direct Mail");
		WebElement src1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropDown1= new Select(src1); 
		List<WebElement>options= dropDown1.getOptions();
		int size= options.size();
		dropDown1.selectByIndex(size-2);
		WebElement src2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dropDown2 = new Select(src2); 
		dropDown2.selectByIndex(2);
		
		driver.findElementByName("submitButton").click();

	}

}
