package week3Day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowSnaps {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		
		//second window
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allwindows= driver.getWindowHandles();
		List<String> listofwindow = new ArrayList<>();
		listofwindow.addAll(allwindows);
		driver.switchTo().window(listofwindow.get(1));
		System.out.println("title of sec window"+driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		
		//snapshot
		File src= driver.getScreenshotAs(OutputType.FILE);
		File desc =new File("./snaps/img.png");
		FileUtils.copyFile (src,desc);
		
		// close first window
		driver.switchTo().window(listofwindow.get(0));
		driver.close();
		
		

	}

}
