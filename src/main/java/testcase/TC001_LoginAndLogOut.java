package testcase;

import org.junit.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC001_LoginAndLogOut extends SeMethods{
	@org.testng.annotations.BeforeClass
	public void setData() {
		testCaseName = "TC_CreateLead";
		testCaseDesc = "Create a New Lead";
		category = "smoke";
		author = "shri";
//		fileName = "Pages";	
}

	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCrm = locateElement("linkText","CRM/SFA");
		click(eleCrm);
		WebElement elecret = locateElement("linkText","Create Lead");
		click(elecret);
		WebElement eleCompanyName = locateElement("id","createLeadForm_companyName");
		type(eleCompanyName,"Emmrc");
		WebElement elefirstName = locateElement("id","createLeadForm_firstName");
		type(elefirstName,"Kishore");
		WebElement ele = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(ele, "Direct Mail");
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
		
	}

	
}







