package week5.day1;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By.ByLinkText;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC001_CreateLead extends ProjectMethods {
	//public static void main(String[] args) {

	//}


	@BeforeTest

	public void setData() { testCaseName = "TC001_CreateLead"; testCaseDesc =
	"Create a new Lead"; category = "smoke"; author = "kishore"; }

	private static final String ByLinkText = null;

	@Test

	public void login() {



		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCrm = locateElement("linkText", "CRM/SFA");
		click(eleCrm);
		WebElement elecret = locateElement("linkText", "Create Lead");
		click(elecret);
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, "Emmrc");
		WebElement elefirstName = locateElement("id", "createLeadForm_firstName");
		type(elefirstName, "Kishore");
		WebElement ele = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(ele, "Direct Mail");
		
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);

	}
	
	private void click(WebElement elesource, String string) {
		// TODO Auto-generated method stub

	}

	private WebElement locateElementByLinkText(String string) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
