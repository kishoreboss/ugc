package week5.day1;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC002_DeleteLead extends ProjectMethods {
	
	  @BeforeTest
	 
	public void setData() {
		testCaseName = "TC002_DeleteLead";
		testCaseDesc = "Create a new Lead";
		category = "smoke";
		author = "kishore";		
	  }
@Test	
public void deletelead() throws InterruptedException {

startApp("chrome", "http://leaftaps.com/opentaps");
WebElement eleUserName= locateElement("id", "username");
type(eleUserName, "demosalesmanager");

WebElement elePassword = locateElement("id", "password");
type(elePassword, "crmsfa");

WebElement eleSubmit = locateElement("class", "decorativeSubmit");
click(eleSubmit);
/*
 		Thread.sleep(2000);
		String leadid = driver.findElementByXPath("(//a[@class='linkText'])[4]").getText();
		System.out.println("Lead ID of First Resulting Lead: " +leadid);
		driver.findElementByXPath("(//a[@class='linkText'])[4]").click();
		
		//delete
		driver.findElementByClassName("subMenuButtonDangerous").click();
		System.out.println("Deleted");
 */

WebElement eleCrmsfa = locateElement("linkText", "CRM/SFA");
click(eleCrmsfa);

WebElement eleLeads = locateElement("linkText", "Leads");
click(eleLeads);

WebElement eleFindLeads = locateElement("linkText", "Find Leads");
click(eleFindLeads);

WebElement elePhone = locateElement("xpath", "//span[text()='Phone']");
click(elePhone);

WebElement eleTypePhoneNo = locateElement("name", "phoneNumber");
type(eleTypePhoneNo, "9999888877");

WebElement eleFindLeadClick = locateElement("xpath", "//button[text()='Find Leads']");
click(eleFindLeadClick);
Thread.sleep(20000);

WebElement eleGetText = locateElement("xpath", "(//a[@class='linkText'])[4]");
String text = getText(eleGetText);
System.out.println("Lead id to be deleted:" +text);

WebElement eleSelectFirstLead = locateElement("xpath", "(//a[@class='linkText'])[4]");
click(eleSelectFirstLead);

driver.findElementByClassName("subMenuButtonDangerous").click();
System.out.println("Deleted");

WebElement eleClickDelete = locateElement("class", "subMenuButtonDangerous");
click(eleClickDelete);
System.out.println("Deleted");

}
}
