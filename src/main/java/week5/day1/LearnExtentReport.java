package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReport {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest test= extent.createTest("TC001_LoginAndLogOut","create a new Lead in leaftaps");
		test.assignCategory("SmokeTest");
		test.assignAuthor("Kishore");
		test.pass("browser Launched Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("The data DEmoSalesManager extent Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("The data crmsfa extent Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.pass("The data Login button clicked Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
		
		extent.flush();
	}

}
