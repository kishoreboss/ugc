package week5.day2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.graphbuilder.math.func.MaxFunction;

public class ProjectDay {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai");
		driver.findElementByClassName("search").click();
		driver.findElementByClassName("items").click();
		driver.findElementByClassName("proceed").click();
		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd"); 
		String today = sdf.format(date);
		int tomorrow = Integer.parseInt(today)+1;
		System.out.println(tomorrow);
		List<WebElement> lst = driver.findElementsByXPath("//div[@class='day']");
		for (WebElement item : lst) {
			if(item.getText().contains(Integer.toString(tomorrow))) {
				System.out.println(item.getText());
				item.click();	
			}
			
		}
		driver.findElementByClassName("proceed").click();
		driver.findElementByClassName("proceed").click();
		Thread.sleep(10000);
		List<WebElement> lstcar = driver.findElementsByXPath("//div[@class='price']");
		System.out.println(lstcar.size());
		
		List<String> lstprice = new ArrayList<String>();
		for (WebElement price : lstcar) {
			//System.out.println(price.getText());
			lstprice.add(price.getText().trim().replaceAll("\\D", ""));
			
			
		}
		System.out.println(lstprice);
		Collections.sort(lstprice);
		int size = lstprice.size();
		String max = lstprice.get(size-1);
		//System.out.println("MAX VALUE"+max);
		//System.out.println("//div[text()[contains(.,'"+max+"')]]");
		System.out.println(" Brand Name of the Max car value:"+driver.findElementByXPath("//div[text()[contains(.,'"+max+"')]]/../../..//following::div/h3").getText());
		driver.findElementByXPath("//div[text()[contains(.,'"+max+"')]]/following::button").click();
		

	}

}
