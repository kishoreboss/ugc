package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id ="createLeadForm_companyName" )
	WebElement elecmpyname;
	public CreateLeadPage typecompyname(String data) {
		type(elecmpyname, data);
		return this;
		
	}
	@FindBy(id ="createLeadForm_firstName" )
	WebElement elefrstname;
	public CreateLeadPage typefrstname(String data) {
		type(elefrstname, data);
		return this;
		
	}
	@FindBy(id ="createLeadForm_lastName" )
	WebElement elelastname;
	public CreateLeadPage typelastname(String data) {
		type(elelastname, data);
		return this;
	}
	@FindBy(id ="createLeadForm_firstNameLocal")
	WebElement elefrstLocal;
	public CreateLeadPage typefrstLocal(String data) {
		type(elefrstLocal, data);
		return this;
	}
	@FindBy(id ="createLeadForm_lastNameLocal")
	WebElement eleLstLocal;
	public CreateLeadPage typelastlocal(String data) {
		type(eleLstLocal, data);
		return this;
	}
	@FindBy(id ="createLeadForm_annualRevenue")
	WebElement eleannualRev;
	public CreateLeadPage typeannualRev(String data) {
		type(eleannualRev, data);
		return this;
	}
	@FindBy(className ="smallSubmit")
	WebElement eleClickCreateLead;
	public ViewLeadPage clickCeateLeadss() {
		click(eleClickCreateLead);
		return new ViewLeadPage();
	}

}
