package Pages;

import org.junit.Before;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage () {
		PageFactory.initElements(driver, this);
				
	}
//	//Before Intergrate using cucumber
//	@FindBy(id="username")
//	WebElement eleusername;
//	public LoginPage typeusername(String data) {
//		type(eleusername, data);
//		return this;
//	}
//	@FindBy(id="password")
//	WebElement elepassword;
//	public LoginPage typepassword(String data) {
//		type(elepassword, data);
//		return this;
//	}
//	@FindBy(className ="decorativeSubmit")
//	WebElement eleLogin;
//	public HomePage clickLogin() {
//		click(eleLogin);
//		return new HomePage();
//	}
//	
//
//}
	
	//After Intergrate with cucumber

	@FindBy(id="username")
	WebElement eleusername;
	@And("Enter the User Name as (.*)")
	public LoginPage typeusername(String data) {
		type(eleusername, data);
		return this;
	}
	@FindBy(id="password")
	WebElement elepassword;
	@And("Enter the Password as (.*)")
	public LoginPage typepassword(String data) {
		type(elepassword, data);
		return this;
	}
	@FindBy(className ="decorativeSubmit")
	WebElement eleLogin;
	@When("Cick on the login button")
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}
	

}
	
