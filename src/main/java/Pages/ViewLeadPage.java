package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{
	public ViewLeadPage() {
			PageFactory.initElements(driver, this);
		}
	@FindBy(id ="createLeadForm_companyName" )
	WebElement verifycmpyname;
	public ViewLeadPage eleverify(String data) {
		verifyExactText(verifycmpyname, data);
		return this;
	}

}
