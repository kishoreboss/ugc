package Pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
public class CreateLeadUsingPages extends ProjectMethods {

		@BeforeTest(groups = {"regression"})
		public void setData() {
			testCaseName = "TC_CreateLead";
			testCaseDesc = "Create a New Lead";
			category = "smoke";
			author = "shri";
			fileName = "Pages";
			
		
		
		
	}
		@Test(dataProvider ="setData")
		public void createPages(String un, String pwd,String cmpyname, String FrstName, String LastName ) {
			new LoginPage()
			.typeusername(un)
			.typepassword(pwd)
			.clickLogin()
			.clickcrmsfa()
			.clickCreateLead()
			.clickCreateLeads()
			.typecompyname(cmpyname)
			.typefrstname(FrstName);
			/*.typelastname(LastName).typefrstLocal(elefrstLocal)
			.type
			.clickCeateLeadss()
			.eleverify(FrstName);	
			*/
		}
	

}
