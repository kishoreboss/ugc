package PagesDelt;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadPage extends ProjectMethods {
	public MyLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Find Leads")
	WebElement eleFindLead;
	public FindLeadPage clickCreateLeads() {
		click(eleFindLead);
		return new FindLeadPage();
	}

	
}
