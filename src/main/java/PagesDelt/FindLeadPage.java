package PagesDelt;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Pages.CreateLeadPage;
import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{
	public FindLeadPage(){
		PageFactory.initElements(driver, this);		
	}
	@FindBy(className =" x-form-text x-form-field ")
	WebElement eleFrstName;
	public FindLeadPage typefrstname(String data) {
		type(eleFrstName, data);
		return this;
	}
	@FindBy(className ="  x-form-text x-form-field  ")
	WebElement eleLrstName;
	public FindLeadPage typeLstname(String data) {
		type(eleLrstName, data);
		return this;
	}	
	@FindBy(className =" x-form-text x-form-field ")
	WebElement eleleadid;
	public FindLeadPage typeleadid(String data) {
		type(eleleadid, data);
		return this;
	}
//	@FindBy(className ="")
}
