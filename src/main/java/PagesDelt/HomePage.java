package PagesDelt;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="CRM/SFA")
	WebElement eleLt;
	public MyHomePage clickcrmsfa() {
		click(eleLt);
		return new MyHomePage();
		
	}
	

}
