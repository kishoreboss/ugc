package PagesDelt;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods {

	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText = "Leads")
	//@FindBy(xpath = "//a[text()='Create Lead']")
	WebElement eleLeads;
	public MyLeadPage clickCreateLead() {
		click(eleLeads);
		return new MyLeadPage();
	}
}
