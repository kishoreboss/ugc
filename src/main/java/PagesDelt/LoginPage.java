package PagesDelt;

import org.junit.Before;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage () {
		PageFactory.initElements(driver, this);
				
	}
	@FindBy(id="username")
	WebElement eleusername;
	public LoginPage typeusername(String data) {
		type(eleusername, data);
		return this;
	}
	@FindBy(id="password")
	WebElement elepassword;
	public LoginPage typepassword(String data) {
		type(elepassword, data);
		return this;
	}
	@FindBy(className ="decorativeSubmit")
	WebElement eleLogin;
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}
	

}
