package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6.day2.ReadExcel;

public class ProjectMethods extends SeMethods{
	public String fileName;
	@DataProvider(name ="positive")
	public Object[][] fetchData() throws IOException{
		System.out.println("data");
		return ReadExcel.getData(fileName);


	}
	@BeforeSuite(groups = {"common"})
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass(groups = {"common"})
	public void beforeClass() {
		startTestCase();
	}
//	@Parameters({"url"})
	@BeforeMethod(groups = {"common"})
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
	}
	@AfterMethod(groups = {"common"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups = {"common"})
	public void afterSuite() {
		endResult();
	}
	@DataProvider(name="setData")
	public Object[][] dataFromExcel() throws IOException{
		return ReadExcel.getData(fileName);
	}
	







}
