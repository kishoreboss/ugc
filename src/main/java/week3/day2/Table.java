package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Table {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/table.html");
		List<WebElement> allcheckbox = driver.findElementsByXPath("//input[@type='checkbox']");
		System.out.println(allcheckbox.size());
		allcheckbox.get(allcheckbox.size()-1).click();
		WebElement find80 = driver.findElementByXPath("//font[contains(text(),'80%')]");
		//find80.click();

		String toClick = "80%";
		String text="";



		// locate table
		WebElement table = driver.findElementByXPath("//table/tbody");

		List<WebElement> tr = table.findElements(By.tagName("tr"));
		for (int i = 1; i <= tr.size()-1; i++) {
			List<WebElement> findElement = tr.get(i).findElements(By.tagName("td"));
			text = findElement.get(1).getText();
			if (text.equals(toClick)) {
				driver.findElementByXPath("//font[contains(text(),'"+text+"')]/following::input[1]").click();
			}
		}

	}

}
