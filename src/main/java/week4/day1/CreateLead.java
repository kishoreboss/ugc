package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead 
{
public static void main(String[] args) throws InterruptedException 
{
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	driver.get("http://leaftaps.com/opentaps");
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	Thread.sleep(3000);
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByXPath("//a[text()='Create Lead']").click();
	Thread.sleep(1000);
	driver.findElementById("createLeadForm_companyName").sendKeys("Emmrc");
	driver.findElementById("createLeadForm_firstName").sendKeys("Kishore");
	driver.findElementById("createLeadForm_lastName").sendKeys("Deva");
	driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Kishore");
	driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Deva");
	driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");
	WebElement sor=driver.findElementById("createLeadForm_dataSourceId");
	Select source=new Select(sor);
	source.selectByVisibleText("Employee");
	driver.findElementById("createLeadForm_generalProfTitle").sendKeys("MR");
	driver.findElementById("createLeadForm_annualRevenue").sendKeys("250000");
	WebElement ind=driver.findElementById("createLeadForm_industryEnumId");
	Select indus=new Select(ind);
	indus.selectByVisibleText("Finance");
	WebElement own = driver.findElementById("createLeadForm_ownershipEnumId");
	Select owner=new Select(own);
	owner.selectByVisibleText("Corporation");
	driver.findElementById("createLeadForm_sicCode").sendKeys("12300");
	driver.findElementById("createLeadForm_description").sendKeys("create lead automation script");
	driver.findElementById("createLeadForm_importantNote").sendKeys("Automation Testing");
	driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
	driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("44");
	driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("45678");
	driver.findElementById("createLeadForm_departmentName").sendKeys("Automation");
	WebElement cur = driver.findElementById("createLeadForm_currencyUomId");
	Select currency=new Select(cur);
	currency.selectByVisibleText("INR - Indian Rupee");
	driver.findElementById("createLeadForm_numberEmployees").sendKeys("500");
	driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Macho");
	driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Murali");
	driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.ibm.com");
	driver.findElementById("createLeadForm_generalToName").sendKeys("Pushparaj");
	driver.findElementById("createLeadForm_generalAddress1").sendKeys("No:499 Nethaji Nagar");
	WebElement con = driver.findElementById("createLeadForm_generalCountryGeoId");
	Select country= new Select(con);
	country.selectByVisibleText("India");
	Thread.sleep(1000);
	WebElement st = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
	Select state=new Select(st);
	state.selectByVisibleText("TAMILNADU");
	driver.findElementById("createLeadForm_generalCity").sendKeys("Tindivanam");
	driver.findElementById("createLeadForm_generalPostalCode").sendKeys("604001");
	WebElement mar = driver.findElementById("createLeadForm_marketingCampaignId");
	Select market=new Select(mar);
	market.selectByIndex(3);
	driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("8144185366");
	driver.findElementById("createLeadForm_primaryEmail").sendKeys("gengusamy.d@gmail.com");
	driver.findElementByXPath("//input[@name='submitButton']").click();
	String firstname="Kishore";
	if(firstname.equals(driver.findElementById("viewLead_firstName_sp").getText()))
	{
		System.out.println("Lead Created");
	}
	
	
}
}
