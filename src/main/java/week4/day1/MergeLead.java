package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead 
{
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		Thread.sleep(3000);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//table[@class='twoColumnForm']//following::td[2]//following::a)[1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> lst=new ArrayList<String>();
		lst.addAll(windowHandles);
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		driver.findElementByXPath("(//label[text()='Lead ID:']//following::input)[1]").sendKeys("10304");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//table[@class='x-grid3-row-table']//following::td[2]/div/a)[1]").click();
		driver.switchTo().window(lst.get(0));
		driver.findElementByXPath("//table[@class='twoColumnForm']/tbody/tr[2]/td[2]/a").click();
		Set<String> w2 = driver.getWindowHandles();
		List<String> lst2=new ArrayList<String>();
		lst2.addAll(w2);
		System.out.println(lst2);
		driver.switchTo().window(lst2.get(1));
		Thread.sleep(2000);
		driver.findElementByXPath("(//label[text()='Lead ID:']//following::input)[1]").sendKeys("10309");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//table[@class='x-grid3-row-table']//following::td[2]/div/a)[1]").click();
		driver.switchTo().window(lst2.get(0));
		driver.findElementByXPath("//table[@class='twoColumnForm']/tbody/tr[4]/td[2]/a").click();
		driver.switchTo().alert().accept();
		Thread.sleep(2000);
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//label[text()='Lead ID:']//following::div/input)[1]").sendKeys("10304");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(1000);
		String text = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		System.out.println(text);
		if(text.equals("No records to display"))
		{
			System.out.println("Error message displayed");
		}
		driver.close();
	}

}
