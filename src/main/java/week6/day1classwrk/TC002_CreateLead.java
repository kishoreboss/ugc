package week6.day1classwrk;

import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import week6.day2.ReadExcel;


public class TC002_CreateLead extends ProjectMethods {

	@BeforeTest(groups = {"smoke"})
	public void setData() {
		testCaseName = "TC_CreateLead";
		testCaseDesc = "Create a New Lead";
		category = "smoke";
		author = "shri";
		fileName ="CreatLead";
	}

	@Test(groups = {"smoke"}, dataProvider = "positive")
	public void createLead(String cmpy,String frst, String lst){
		WebElement cld = locateElement("linkText","Create Lead");
		click(cld);
		WebElement cname = locateElement("id", "createLeadForm_companyName");
		type(cname, cmpy);
		WebElement fname = locateElement("id", "createLeadForm_firstName");
		type(fname, frst);
		WebElement lname = locateElement("id", "createLeadForm_lastName");
		type(lname, lst);
		WebElement cnamel = locateElement("id", "createLeadForm_firstNameLocal");
		type(cnamel, "nil");
		WebElement fnamel = locateElement("id", "createLeadForm_lastNameLocal");
		type(fnamel, "nil");
		WebElement lnamel = locateElement("id", "createLeadForm_personalTitle");
		type(lnamel, "ck");
		WebElement dsour = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(dsour, "Direct Mail");
		WebElement prof = locateElement("id", "createLeadForm_generalProfTitle");
		type(prof, "Mr");
		WebElement rev = locateElement("id", "createLeadForm_annualRevenue");
		type(rev, "10 Lakhs");
		WebElement ind = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(ind, 2);
		WebElement ownr = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ownr, "OWN_PUBLIC_CORP");	
		WebElement sic = locateElement("id", "createLeadForm_sicCode");
		type(sic, "SIC sample");
		WebElement desc = locateElement("id", "createLeadForm_description");
		type(desc, "SIC description");
		WebElement imp = locateElement("id", "createLeadForm_importantNote");
		type(imp, "Important Note");
		WebElement code = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		type(code, "91");
		WebElement area = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(area, "044");
		WebElement ext = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(ext, "5678");
		WebElement dep = locateElement("id", "createLeadForm_departmentName");
		type(dep, "IT");
		WebElement curr = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(curr, "INR - Indian Rupee");

		WebElement tick = locateElement("id", "createLeadForm_tickerSymbol");
		type(tick, "Sample");
		WebElement ask = locateElement("id", "createLeadForm_primaryPhoneAskForName");
		type(ask, "kumar");
		WebElement gen = locateElement("id", "createLeadForm_generalToName");
		type(gen, "Maari");
		WebElement gadd = locateElement("id", "createLeadForm_generalAddress1");
		type(gadd, "Vivekanandhar Theru");
		WebElement gaddr = locateElement("id", "createLeadForm_generalAddress2");
		type(gaddr, "Dubai Kurukku Sandhu");
		WebElement gcity = locateElement("id", "createLeadForm_generalCity");
		type(gcity, "Abudhabi");
		WebElement ctry = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(ctry, "India");
		WebElement state = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		explicitWait(20, state);
		selectDropDownUsingText(state, "IN-TN");
		WebElement post = locateElement("id", "createLeadForm_generalPostalCode");
		type(post, "6040011");
		WebElement pext = locateElement("id", "createLeadForm_generalPostalCodeExt");
		type(pext, "12");
		WebElement camp = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(camp, 7);
		WebElement phone = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(phone, "8144185366");
		WebElement mail = locateElement("id", "createLeadForm_primaryEmail");
		type(mail, "gengusamy.d@gmail.com");
		WebElement submit = locateElement("class", "smallSubmit");
		click(submit);
		WebElement vname = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(vname, "kishore");


	}


	//@DataProvider(name ="negative")
	//public void fetchNegData() {

	//}

	private void explicitWait(int i, WebElement state) {
		// TODO Auto-generated method stub

	}

}
