package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class DuplicateChar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String txt = "Giri Tharan";
		char[] ch =txt.toCharArray();
		Map<Character,Integer> map = new HashMap<Character,Integer>();
		for (char c : ch) {
			if(map.containsKey(c)) {
				Integer val =map.get(c)+1;
				map.put(c, val);
			}else {
				map.put(c, 1);
			}
		}
		System.out.println(map);
		System.out.println(map.keySet());
		for (char c : map.keySet()) {
			if(map.get(c)>1)
			{
				System.out.println("duplicate character:"+c+" "+map.get(c));
			}
			
		}

	}

}
