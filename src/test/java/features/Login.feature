Feature: LogIn for Leaftaps

#Background:
#Given Open the Chrome Browser
#And maximize the Browser
#And Set Timeouts
#And Hit the URL

#without data
#Scenario: Positive Login Flow
#
#Given Open the Chrome Browser
#And maximize the Browser
#And Set Timeouts
#And Hit the URL
#And Enter the User Name
#And Enter the Password
#When Cick on the login button
#Then verify the login is success

#With data
#Scenario: Positive Login Flow
#
#Given Open the Chrome Browser
#And maximize the Browser
#And Set Timeouts
#And Hit the URL
#And Enter the User Name as DemoSalesManager
#And Enter the Password as crmsfa
#When Cick on the login button
#Then verify the login is success

#Using Background to reduce representative
@Positive 
Scenario Outline: Positive Login Flow


And Enter the User Name as <UserName>
And Enter the Password as <Password>
When Cick on the login button
#Then verify the login is success
Examples:
|UserName|Password|
|DemoSalesManager|crmsfa|
|DemoCSR|crmsfa|

#@Positive @Negative
#Scenario Outline: Positive Login Flow
#
#
#And Enter the User Name as <UserName>
#And Enter the Password as <Password>
#When Cick on the login button
#But verify the login is success
#Examples:
#|UserName|Password|
#|DemoSalesManager|crmsfa1|
